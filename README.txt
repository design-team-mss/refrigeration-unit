Refrigeration unit piHAT for use with raspberryPi.

The piHAT consists of 3 main subsystems - a power supply, amplifier,  and status LEDs, and interfaces with a mother raspberryPi via a 40 pin GPIO
header. 

Usage:

For use in domestic or commercial environments.
