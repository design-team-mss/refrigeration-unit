EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Amplifier_Operational:LM741 U1
U 1 1 60B97CEE
P 4100 3300
F 0 "U1" H 4444 3346 50  0000 L CNN
F 1 "LM741" H 4444 3255 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:OnSemi_ECH8" H 4150 3350 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm741.pdf" H 4250 3450 50  0001 C CNN
	1    4100 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 60B99C17
P 3350 3400
F 0 "R7" V 3143 3400 50  0000 C CNN
F 1 "1000" V 3234 3400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 3280 3400 50  0001 C CNN
F 3 "~" H 3350 3400 50  0001 C CNN
	1    3350 3400
	0    1    1    0   
$EndComp
$Comp
L Amplifier_Operational:LM741 U2
U 1 1 60B9CC7D
P 5700 3200
F 0 "U2" H 6044 3246 50  0000 L CNN
F 1 "LM741" H 6044 3155 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:OnSemi_ECH8" H 5750 3250 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm741.pdf" H 5850 3350 50  0001 C CNN
	1    5700 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 3400 3700 3400
Wire Wire Line
	3700 3400 3700 4000
Connection ~ 3700 3400
Wire Wire Line
	3700 3400 3800 3400
Wire Wire Line
	4150 4000 4600 4000
Wire Wire Line
	4600 4000 4600 3300
Wire Wire Line
	4400 3300 4600 3300
Connection ~ 4600 3300
Wire Wire Line
	4600 3300 4800 3300
Wire Wire Line
	5100 3300 5250 3300
Wire Wire Line
	5250 3300 5250 4000
Wire Wire Line
	5250 4000 5500 4000
Connection ~ 5250 3300
Wire Wire Line
	5250 3300 5400 3300
Wire Wire Line
	5800 4000 6350 4000
Wire Wire Line
	6350 4000 6350 3200
Wire Wire Line
	6350 3200 6000 3200
Wire Wire Line
	6350 3200 6500 3200
Connection ~ 6350 3200
Wire Wire Line
	5400 3100 5250 3100
Wire Wire Line
	5250 3100 5250 2500
Wire Wire Line
	5250 2500 4600 2500
Wire Wire Line
	3800 2500 3800 3200
$Comp
L power:GND #PWR0116
U 1 1 60B9EB2C
P 4600 2500
F 0 "#PWR0116" H 4600 2250 50  0001 C CNN
F 1 "GND" H 4605 2327 50  0000 C CNN
F 2 "" H 4600 2500 50  0001 C CNN
F 3 "" H 4600 2500 50  0001 C CNN
	1    4600 2500
	1    0    0    -1  
$EndComp
Connection ~ 4600 2500
Wire Wire Line
	4600 2500 3800 2500
Text GLabel 3200 3400 0    50   Input ~ 0
V_regulator
Text GLabel 6500 3200 2    50   Output ~ 0
V_amplifier
Text GLabel 5600 3500 0    50   Input ~ 0
V_-15
Text GLabel 4000 3600 0    50   Input ~ 0
V_-15
Text GLabel 5600 2900 0    50   Input ~ 0
V_+15
Text GLabel 4000 3000 2    50   Input ~ 0
V_+15
$Comp
L Device:R R8
U 1 1 60BDB893
P 4000 4000
F 0 "R8" V 3793 4000 50  0000 C CNN
F 1 "660" V 3884 4000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 3930 4000 50  0001 C CNN
F 3 "~" H 4000 4000 50  0001 C CNN
	1    4000 4000
	0    1    1    0   
$EndComp
$Comp
L Device:R R10
U 1 1 60BDBC8F
P 5650 4000
F 0 "R10" V 5443 4000 50  0000 C CNN
F 1 "1000" V 5534 4000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 5580 4000 50  0001 C CNN
F 3 "~" H 5650 4000 50  0001 C CNN
	1    5650 4000
	0    1    1    0   
$EndComp
$Comp
L Device:R R9
U 1 1 60BDC170
P 4950 3300
F 0 "R9" V 4743 3300 50  0000 C CNN
F 1 "1000" V 4834 3300 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 4880 3300 50  0001 C CNN
F 3 "~" H 4950 3300 50  0001 C CNN
	1    4950 3300
	0    1    1    0   
$EndComp
Wire Wire Line
	3700 4000 3850 4000
$EndSCHEMATC
