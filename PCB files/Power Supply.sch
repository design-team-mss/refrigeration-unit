EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 1N4733A:1N4733A Z1
U 1 1 60B900DD
P 6000 3000
F 0 "Z1" V 6254 3130 50  0000 L CNN
F 1 "1N4733A" V 6345 3130 50  0000 L CNN
F 2 "DIOAD1380W86L436D236" H 6400 3150 50  0001 L CNN
F 3 "https://www.mouser.com/ProductDetail/Central-Semiconductor/1N4733A?qs=sGAEpiMZZMuSK2mCDyT9chevn2%252ByjySH" H 6400 3050 50  0001 L CNN
F 4 "Zener Diodes 1W Zener Diode 5.1V 550Ohm 1.0Vr" H 6400 2950 50  0001 L CNN "Description"
F 5 "" H 6400 2850 50  0001 L CNN "Height"
F 6 "Central Semiconductor" H 6400 2750 50  0001 L CNN "Manufacturer_Name"
F 7 "1N4733A" H 6400 2650 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "610-1N4733A" H 6400 2550 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=610-1N4733A" H 6400 2450 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 6400 2350 50  0001 L CNN "Arrow Part Number"
F 11 "" H 6400 2250 50  0001 L CNN "Arrow Price/Stock"
	1    6000 3000
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 60B92192
P 5350 3000
F 0 "R1" V 5143 3000 50  0000 C CNN
F 1 "913 000" V 5234 3000 50  0000 C CNN
F 2 "" V 5280 3000 50  0001 C CNN
F 3 "~" H 5350 3000 50  0001 C CNN
	1    5350 3000
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 60B93D79
P 6800 3350
F 0 "R2" H 6870 3396 50  0000 L CNN
F 1 "100 000" H 6870 3305 50  0000 L CNN
F 2 "" V 6730 3350 50  0001 C CNN
F 3 "~" H 6800 3350 50  0001 C CNN
	1    6800 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 3000 6000 3000
Wire Wire Line
	6800 3200 6800 3000
Wire Wire Line
	6800 3000 6000 3000
Connection ~ 6000 3000
Wire Wire Line
	6800 3500 6800 3600
Wire Wire Line
	6800 3600 6000 3600
Wire Wire Line
	6000 3600 5000 3600
Connection ~ 6000 3600
Wire Wire Line
	5200 3000 5000 3000
$Comp
L power:GND #PWR?
U 1 1 60B964C5
P 6000 3600
F 0 "#PWR?" H 6000 3350 50  0001 C CNN
F 1 "GND" H 6005 3427 50  0000 C CNN
F 2 "" H 6000 3600 50  0001 C CNN
F 3 "" H 6000 3600 50  0001 C CNN
	1    6000 3600
	1    0    0    -1  
$EndComp
Text GLabel 6800 3000 2    50   Output ~ 0
V_regulator
Wire Wire Line
	5000 3000 5000 3300
$Comp
L 1N5352BG:1N5352BG Z2
U 1 1 60BA5506
P 6000 4000
F 0 "Z2" V 6254 4130 50  0000 L CNN
F 1 "1N5352BG" V 6345 4130 50  0000 L CNN
F 2 "DIOAD2260W109L864D349" H 6400 4150 50  0001 L CNN
F 3 "http://docs-emea.rs-online.com/webdocs/0b21/0900766b80b2115d.pdf" H 6400 4050 50  0001 L CNN
F 4 "Zener Diode, 15V 5% 5 W Through Hole 2-pin DO-15" H 6400 3950 50  0001 L CNN "Description"
F 5 "" H 6400 3850 50  0001 L CNN "Height"
F 6 "ON Semiconductor" H 6400 3750 50  0001 L CNN "Manufacturer_Name"
F 7 "1N5352BG" H 6400 3650 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "863-1N5352BG" H 6400 3550 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/ON-Semiconductor/1N5352BG?qs=y2kkmE52mdMNK4AcWRFeCA%3D%3D" H 6400 3450 50  0001 L CNN "Mouser Price/Stock"
F 10 "1N5352BG" H 6400 3350 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/1n5352bg/on-semiconductor" H 6400 3250 50  0001 L CNN "Arrow Price/Stock"
	1    6000 4000
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 60BA797B
P 5350 4000
F 0 "R3" V 5143 4000 50  0000 C CNN
F 1 "913 000" V 5234 4000 50  0000 C CNN
F 2 "" V 5280 4000 50  0001 C CNN
F 3 "~" H 5350 4000 50  0001 C CNN
	1    5350 4000
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 60BA8314
P 6800 4350
F 0 "R4" H 6870 4396 50  0000 L CNN
F 1 "100000" H 6870 4305 50  0000 L CNN
F 2 "" V 6730 4350 50  0001 C CNN
F 3 "~" H 6800 4350 50  0001 C CNN
	1    6800 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 4000 5000 4000
Wire Wire Line
	5000 4000 5000 4300
Wire Wire Line
	5000 4600 6000 4600
Wire Wire Line
	6000 4600 6800 4600
Wire Wire Line
	6800 4600 6800 4500
Connection ~ 6000 4600
Wire Wire Line
	6800 4200 6800 4000
Wire Wire Line
	6800 4000 6000 4000
Wire Wire Line
	5500 4000 6000 4000
Connection ~ 6000 4000
Wire Wire Line
	4650 4300 5000 4300
Connection ~ 5000 4300
Wire Wire Line
	5000 4300 5000 4600
Wire Wire Line
	4650 3300 5000 3300
Connection ~ 5000 3300
Wire Wire Line
	5000 3300 5000 3600
Text GLabel 6800 4000 2    50   Output ~ 0
V_+15
$Comp
L Device:Battery BT1
U 1 1 60BAD56C
P 4100 4350
F 0 "BT1" H 4208 4396 50  0000 L CNN
F 1 "50V" H 4208 4305 50  0000 L CNN
F 2 "" V 4100 4410 50  0001 C CNN
F 3 "~" V 4100 4410 50  0001 C CNN
	1    4100 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 3300 4650 3800
Wire Wire Line
	4100 4150 4100 3800
Wire Wire Line
	4100 3800 4650 3800
Connection ~ 4650 3800
Wire Wire Line
	4650 3800 4650 4300
$Comp
L power:GND #PWR?
U 1 1 60BAF5D8
P 4100 4550
F 0 "#PWR?" H 4100 4300 50  0001 C CNN
F 1 "GND" H 4105 4377 50  0000 C CNN
F 2 "" H 4100 4550 50  0001 C CNN
F 3 "" H 4100 4550 50  0001 C CNN
	1    4100 4550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60BAFF93
P 6000 4600
F 0 "#PWR?" H 6000 4350 50  0001 C CNN
F 1 "GND" H 6005 4427 50  0000 C CNN
F 2 "" H 6000 4600 50  0001 C CNN
F 3 "" H 6000 4600 50  0001 C CNN
	1    6000 4600
	1    0    0    -1  
$EndComp
$Comp
L 1N5352BG:1N5352BG Z?
U 1 1 60BB8F48
P 6000 4900
F 0 "Z?" V 6254 5030 50  0000 L CNN
F 1 "1N5352BG" V 6345 5030 50  0000 L CNN
F 2 "DIOAD2260W109L864D349" H 6400 5050 50  0001 L CNN
F 3 "http://docs-emea.rs-online.com/webdocs/0b21/0900766b80b2115d.pdf" H 6400 4950 50  0001 L CNN
F 4 "Zener Diode, 15V 5% 5 W Through Hole 2-pin DO-15" H 6400 4850 50  0001 L CNN "Description"
F 5 "" H 6400 4750 50  0001 L CNN "Height"
F 6 "ON Semiconductor" H 6400 4650 50  0001 L CNN "Manufacturer_Name"
F 7 "1N5352BG" H 6400 4550 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "863-1N5352BG" H 6400 4450 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/ON-Semiconductor/1N5352BG?qs=y2kkmE52mdMNK4AcWRFeCA%3D%3D" H 6400 4350 50  0001 L CNN "Mouser Price/Stock"
F 10 "1N5352BG" H 6400 4250 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/1n5352bg/on-semiconductor" H 6400 4150 50  0001 L CNN "Arrow Price/Stock"
	1    6000 4900
	0    1    1    0   
$EndComp
$Comp
L Device:R R5
U 1 1 60BB8F4E
P 5350 4900
F 0 "R5" V 5143 4900 50  0000 C CNN
F 1 "913 000" V 5234 4900 50  0000 C CNN
F 2 "" V 5280 4900 50  0001 C CNN
F 3 "~" H 5350 4900 50  0001 C CNN
	1    5350 4900
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 60BB8F54
P 6800 5250
F 0 "R6" H 6870 5296 50  0000 L CNN
F 1 "100000" H 6870 5205 50  0000 L CNN
F 2 "" V 6730 5250 50  0001 C CNN
F 3 "~" H 6800 5250 50  0001 C CNN
	1    6800 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 4900 5000 4900
Wire Wire Line
	5000 4900 5000 5200
Wire Wire Line
	5000 5500 6000 5500
Wire Wire Line
	6000 5500 6800 5500
Wire Wire Line
	6800 5500 6800 5400
Connection ~ 6000 5500
Wire Wire Line
	6800 5100 6800 4900
Wire Wire Line
	6800 4900 6000 4900
Wire Wire Line
	5500 4900 6000 4900
Connection ~ 6000 4900
Wire Wire Line
	4650 5200 5000 5200
Connection ~ 5000 5200
Wire Wire Line
	5000 5200 5000 5500
Text GLabel 6800 4900 2    50   Output ~ 0
V_-15
$Comp
L power:GND #PWR?
U 1 1 60BB8F68
P 6000 5500
F 0 "#PWR?" H 6000 5250 50  0001 C CNN
F 1 "GND" H 6005 5327 50  0000 C CNN
F 2 "" H 6000 5500 50  0001 C CNN
F 3 "" H 6000 5500 50  0001 C CNN
	1    6000 5500
	1    0    0    -1  
$EndComp
$Comp
L Device:Battery BT?
U 1 1 60BBA7B0
P 4650 5000
F 0 "BT?" H 4758 5046 50  0000 L CNN
F 1 "50V" H 4758 4955 50  0000 L CNN
F 2 "" V 4650 5060 50  0001 C CNN
F 3 "~" V 4650 5060 50  0001 C CNN
	1    4650 5000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60BBAD88
P 4650 4800
F 0 "#PWR?" H 4650 4550 50  0001 C CNN
F 1 "GND" H 4655 4627 50  0000 C CNN
F 2 "" H 4650 4800 50  0001 C CNN
F 3 "" H 4650 4800 50  0001 C CNN
	1    4650 4800
	-1   0    0    1   
$EndComp
$EndSCHEMATC
